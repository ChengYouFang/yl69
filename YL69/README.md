# YL69
- Arduino NANO ?
- Arduino NANO UNO Shield
- YL69 Module

### Arduino NANO UNO Shield
| Name | Mean |
| ------ | ------ |
| G | GND |
| V | [GPIO](https://zh.wikipedia.org/wiki/GPIO) |
| S | Singal |


### YL69 Module
| YL69 | Module |
| ------ | ------ |
| + | + |
| - | - |


### YL69 Module to Arduino NANO UNO Shield
| YL69 Pin | Shield Pin |
| ------ | ------ |
| Vcc | 5V |
| GND | GND |
| D0 | 13-V |
| A0 | A0-S |



### Relay to Arduino NANO UNO Shield
| Relay Pin | Shield Pin |
| ------ | ------ |
| + | 3.3V |
| - | GND |
| N1 | 11-S |
| NO | 3.3V |
| COM | Motor + |


### Motor
| Motor Pin | Pin |
| ------ | ------ |
| - | Shield GND |
| + | Relay COM |