/*
 Name:		YL69.ino
 Created:	5/3/2018 5:21:44 AM
 Author:	ChengYou
*/

//設定YL類比角位
const int YL69 = A0;
//設定YL角位
const int YLD0 = 13;
//馬達角位
const int MOTOR_PIN = 11;

// the setup function runs once when you press reset or power the board
void setup() {
	//設定Baud Rate速度為9600
	Serial.begin(9600);
	//設定YL69D0 OUTPUT
	pinMode(YLD0, OUTPUT);
	//設定馬達 OUTPUT
	pinMode(MOTOR_PIN, OUTPUT);
	digitalWrite(MOTOR_PIN, LOW);
}

// the loop function runs over and over again until power down or reset
void loop() {
	//取得YL69值
	int v = readYL69Value();
	//印出值
	Serial.println(v);

	//判斷式
	if (v > 500) {
		Serial.println("潮濕");
		//關閉馬達
		digitalWrite(MOTOR_PIN, LOW);

	}
	else {
		Serial.println("乾燥");		
		digitalWrite(MOTOR_PIN, HIGH);
		//開啟馬達

	}
	//延遲0.5秒

	delay(500);
}

// read YL69 value
int readYL69Value() {
	//YL69設定高電位
	digitalWrite(YLD0, HIGH);
	//延遲0.5秒
	delay(500);
	//取得類比轉數位數值
	int moist = analogRead(YL69);
	//YL69設定低電位
	digitalWrite(YLD0, LOW);
	//用最大值1023去減數值才能推得正確的值
	return 1023 - moist;
}
